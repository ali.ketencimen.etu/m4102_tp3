package fr.ulille.iut.pizzaland;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.glassfish.jersey.server.ResourceConfig;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    public static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");
        String environment = System.getenv("PIZZAENV");

        if ( true || (environment != null && environment.equals("withdbingredient") )) {
            LOGGER.info("Loading with database");
            try {
                FileReader reader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
                List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>(){}.getClass().getGenericSuperclass());

                IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
                ingredientDao.dropTable();
                ingredientDao.createTable();
                for ( Ingredient ingredient: ingredients) {
                        ingredientDao.insert(ingredient); 
                }
//                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
//                pizzaDao.createTableAndIngredientAssociation();
//                Pizza pizza = new Pizza();
//        		pizza.setName("margherita");
//        		pizza.setSauce("tomate");
//        		List<Ingredient> ingredientsMargherita = new ArrayList<Ingredient>();
//        		ingredients.add(ingredientDao.findByName("tomate"));  ingredients.add(ingredientDao.findByName("fromage"));
//        		pizza.setIngredients(ingredientsMargherita);
//        		pizzaDao.insertwithIngredients(pizza);
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        }
        

        if ( environment != null && environment.equals("withdbpizza") ) {
            LOGGER.info("Loading with database");
            try {
                FileReader reader = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
                List<Pizza> pizzas = JsonbBuilder.create().fromJson(reader, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());

                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropTableAndIngredientAssociation();
                pizzaDao.createTableAndIngredientAssociation();
                for ( Pizza pizza : pizzas) {
                	pizzaDao.insertwithIngredients(pizza); 
                }
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        } 

    }
}
