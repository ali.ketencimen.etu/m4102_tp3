package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private String name;
	private String sauce;
	private List<Ingredient> ingredients;
	
	public Pizza() {}

	public Pizza(String name, String sauce, List<Ingredient> ingredients) {
		this.name = name;
		this.sauce = sauce;
		this.ingredients = ingredients;
	}

	public Pizza(UUID id, String name, String sauce, List<Ingredient> ingredients) {
		this(name, sauce, ingredients);
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSauce() {
		return sauce;
	}

	public void setSauce(String sauce) {
		this.sauce = sauce;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setIngredients(p.ingredients);
		dto.setName(p.getName());
		dto.setSauce(p.getSauce());
		return dto;
	}
	
	public static Pizza fromDto(PizzaDto dto) {
		Pizza p = new Pizza();
		p.setId(dto.getId());
		p.setIngredients(dto.getIngredients());
		p.setName(dto.getName());
		p.setSauce(dto.getSauce());
		return p;
	}
	
	public static PizzaCreateDto toCreateDto(Pizza p) {
		PizzaCreateDto pcd = new PizzaCreateDto();
		pcd.setIngredients(p.getIngredients());
		pcd.setName(p.getName());
		pcd.setSauce(p.getSauce());
		return pcd;
	}
	
	public static Pizza fromPizzaCreateDto(PizzaCreateDto pcd) {
		Pizza p = new Pizza();
		p.setName(pcd.getName());
		p.setIngredients(pcd.getIngredients());
		p.setSauce(pcd.getSauce());
		return p;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sauce == null) ? 0 : sauce.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sauce == null) {
			if (other.sauce != null)
				return false;
		} else if (!sauce.equals(other.sauce))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", sauce=" + sauce + ", ingredients=" + ingredients + "]";
	}
	
	
}
