package fr.ulille.iut.pizzaland.dao;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;


public interface PizzaDao {
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaTable (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, sauce VARCHAR)")
    void createPizzaTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza VARCHAR, idIngredient VARCHAR)")
	void createAssociationTable();
	
	@Transaction
    default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
    }

	@SqlUpdate("DROP TABLE IF EXISTS pizzaTable")
	void dropPizzaTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS pizzaIngredientsAssociation")
	void dropAssociationTable();
	
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropPizzaTable();
		dropAssociationTable();
	}

	@SqlUpdate("INSERT INTO pizzaTable (id, name, sauce) VALUES (:id, :name, :sauce)")
	void insertPizza(@BindBean Pizza pizza);
	
	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:pizzaId, :ingredientId)")
	void insertIngredients(@Bind("pizzaId") UUID pizzaId, @Bind("ingredientId") UUID ingredientId);
	
	@Transaction
	default void insertwithIngredients(Pizza pizza) {
//		System.out.println(pizza.getName() + ":" + pizza.getId() + Arrays.deepToString(pizza.getIngredients().toArray()));
		insertPizza(pizza);
		for(Ingredient i : pizza.getIngredients()) {
			insertIngredients(pizza.getId(), i.getId());
		}
	}

	
	@SqlUpdate("DELETE FROM pizzaTable WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM pizzaTable")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
    
    @SqlQuery("SELECT * FROM pizzaTable WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
    
    @RegisterBeanMapper(Pizza.class)
    Pizza findByNameWithIngredients(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizzaTable WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM pizzaTable WHERE sauce = :sauce")
    @RegisterBeanMapper(Pizza.class)
    Pizza findBySauce(@Bind("sauce") String sauce);
    
    @SqlQuery("SELECT * FROM pizzaTable where id = (SELECT DISTINCT idPizza FROM pizzaIngredientsAssociation where idIngredient = :id)")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> findByIngredient(@BindBean Ingredient ingredient);
    
    @SqlQuery("SELECT ")
    @RegisterBeanMapper(Pizza.class)
    List<Ingredient> getIngredients(Pizza pizza);
}
