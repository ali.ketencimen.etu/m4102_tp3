URI					opération		MIME					Requête		Réponse
/pizzas				GET				<-application/json					liste des pizzas(I2)
									<-applcation/xml
									
/pizzas/{id}		GET				<-application/json					une pizza(I2) ou 404
									<-application/xml
									
/pizzas/{id}/name	GET				<-text/plain						nom de la pizza ou 404

/pizzas				POST			<-/->application/json	pizza(I1)	nouvelle pizza ou 409 si une pizza de même nom existe

/pizzas/{id}		DELETE


une pizza(I2) est représenter par son id, son nom, sa sauce et ses ingredients(une liste):

{
	"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
	"name": "fromage",
	"sauce": "tomate",
	ingredients[{
		"id":"657f8dd4-6bc1-4622-9af7-37d248846a23",
		"name":"fromage"
	} ...]
}

à la création l'id n'est pas connu car fourni par JavaBean qui représente la pizza. on aura donc une représentation JSON(I1) avec seulement le nom, la sauce et les ingrédients:

{
	"name": "fromage",
	"sauce": "tomate",
	ingredients[{
		"id":"657f8dd4-6bc1-4622-9af7-37d248846a23",
		"name":"fromage"
	} ...]
}